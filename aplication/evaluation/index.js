var db    = require('../../config/database');
var util  = require('../util');

var evaluation = {}

evaluation.getEvaluationByCompany = function(request, response) {
    db.query('SELECT * FROM evaluiation, stores, evaluations_by_stores WHERE stores.id_stores ='+ request.params.companyId+' AND evaluations_by_stores.id_store = stores.id_stores AND evaluiation.id_evaluation = evaluations_by_stores.id_evaluation',
      function(error, result){
        response.json(result);
    });
};

evaluation.addEvaluation = function(request, response) {	

  var user = util.descripty(request.headers.authorization);
	var evaluation = {
		score       : request.body.score,
		message     : request.body.description
	};    
  db.connect();
  db.query('INSERT INTO evaluiation SET ?', evaluation,
    function(error, result){
      var users_evaluations = {
        id_user  : user[0].id_users,
        id_evaluation : result.insertId, 
        id_store : request.body.id_companies
      };    
      
      db.query('INSERT INTO evaluations_by_stores SET ?', users_evaluations, function(error, result_evaluation){
        response.json(result_evaluation);
      })
  });
  db.end();
};

evaluation.getEvaluationByUser = function(request, response) {
  if(request.headers.authorization){
    var user = util.descripty(request.headers.authorization);
    if(user[0].type == 1){
      db.connect();
      db.query('SELECT * FROM evaluations, users, companies, users_has_evaluations WHERE users.id_users ='+user[0].id_users+' AND users.status = 1 AND companies.id_companies = users_has_evaluations.companies_id_companies AND evaluations.id_evaluations = users_has_evaluations.evaluations_id_evaluations AND users.id_users = users_has_evaluations.users_id_users',
        function(error, result){
          response.json(result);
      });
      db.end();
    }else if(user[0].type == 2){
      db.connect();
      db.query('SELECT * FROM evaluations, users, companies, users_has_evaluations WHERE users.id_users ='+user[0].id_users+' AND users.status = 1 AND users.id_users = companies.users.id_users AND companies.id_companies = users_has_evaluations.companies_id_companies AND evaluations.id_evaluations = users_has_evaluations.evaluations_id_evaluations',
        function(error, result){
          response.json(result);
      });
      db.end();
    }else if(user[0].type == 3){
      db.connect();
      db.query('SELECT * FROM evaluations WHERE evaluations.status = 0 ',
        function(error, result){
          response.json(result);
      });
      db.end();
    }
  } 
}

evaluation.updateEvaluation = function(request, response) { 
  var evaluation = {
    status      : request.body.status,
  };
  db.connect(); 
  db.query('UPDATE evaluations SET ? WHERE evaluations.id_evaluations=?',[evaluation, request.params.evaluationId], function(error, result){
      response.json(result);
  });
  db.end();
};

evaluation.removeEvaluation = function(request, response) { 
  var evaluation = { status : 0};
  db.connect(); 
  db.query('UPDATE evaluations SET ? WHERE evaluations.id_evaluations=?',[evaluation, request.params.evaluationId], function(error, result){
      response.json(result);
  });
  db.end();
};

module.exports = evaluation;