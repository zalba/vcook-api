var db    = require('../../config/database');
var util  = require('../util');
var user = {}

user.getUser = function(request, response) {
    db.query('SELECT * FROM user WHERE user.status = 1 AND user.id_user ='+ request.params.userId,
      function(error, result){
        response.json(result[0]);
    });
};

user.getAllUser = function(request, response) {
    db.query('SELECT * FROM user WHERE user.status = 1',
      function(error, result){
        response.json(result);
    });
};

user.addUser = function(request, response) {
	var user = {
		email    : request.body.email,
		password : request.body.password,
		name     : request.body.name
	};
    db.query('INSERT INTO user SET ?', user,
      function(error, result){
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.json(result.insertId);
    });
};

user.updateUser = function(request, response) {	    
	var user = {
		email    : request.body.email,
		password : request.body.password,
		name     : request.body.name
	};
    db.connect(); 
	db.query('UPDATE users SET ? WHERE id_user=?',[user, request.params.userId], function(error, result){
        response.json(result);
    });
    db.end();
};

user.removeUser = function(request, response) {	
	var user = { status : 0};
    db.connect(); 
	db.query('UPDATE user SET ? WHERE id_user=?',[user, request.params.userId], function(error, result){
        response.json(result);
    });
    db.end();
};

user.login = function(request, response){   
    db.connect();  
    db.query("SELECT * FROM user WHERE email='"+request.body.email+"' AND password='"+request.body.password+"'", function(error, result){
        if(result =="" )
            response.json({error: 'usuario não encotrado'});
        else
            response.json({ token: util.encripty(result) , user : result[0]});
    });
    db.end();
}

module.exports = user;