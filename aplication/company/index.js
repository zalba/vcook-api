var db = require('../../config/database');

var company = {}

company.getCompany = function(request, response) {
    db.connect();
    db.query('SELECT * FROM stores, filters, filters_by_stores WHERE  stores.id_stores ='+request.params.companyId+' AND  filters.id_filter = filters_by_stores.id_filter AND filters_by_stores.id_store = stores.id_stores',
      function(error, result){
      	response.json(result[0]);        
    });
    db.end();  
};

company.getAllCompany = function(request, response) {
    db.connect();
    db.query('SELECT * FROM stores ',
      function(error, result){        
      	result.map(function(ele){      		
      		db.query('SELECT * FROM filters,filters_by_stores WHERE filters_by_stores.id_store = '+ele.id_store+' AND filters.id_filter = filters_by_stores.id_filter',
	      		function(error, result_filter){
	      			ele.filters = result_filter;   			     	           
              db.query('SELECT * FROM evaluiation,evaluations_by_stores WHERE evaluations_by_stores.id_store = '+ele.id_store+' AND evaluations_by_stores.id_evaluation = evaluiation.id_evaluation',
                function(error, result_evaluation){
                  ele.evaluations = result_evaluation;                      
              }); 
	      	}); 
      	});

        response.json(result);
    });
    db.end();
};

company.getCompaniesByFilter = function(request, response) {    
    var sub_sql = '';
    var filters = request.body.filters
    sub_sql = filters.substring(1,filters.length - 1);
    filters = sub_sql;
    db.connect();
    db.query('SELECT stores.* FROM stores,filters,filters_by_stores WHERE stores.id_stores = filters_by_stores.id_store AND filters_by_stores.id_filter = filters.id_filter AND filters.id_filter IN ('+filters+') GROUP BY id_store',
		function(error, result){
            response.json(result);
    });
    db.end();
}

company.addCompany = function(request, response) {	
	var user = {
  		email    : request.body.email,
  		password : request.body.password,
  		name     : request.body.name,
  		type	 : 2
  	};
    db.connect();
    db.query('INSERT INTO users SET ?', user,
      function(error, result){        
        var company = {
          cep: request.body.cep,
          number: request.body.number,
          neighborhood: request.body.neighborhood,
          street: request.body.street,
          city : request.body.city,
          uf: request.body.uf,
          country: request.body.country,
          lat: 0,
          long: 0,
          phone: request.body.phone,
          price_around: request.body.price_around,
          users_id_users: result.insertId,
        };
        
        db.query('INSERT INTO companies SET ?', company,function(error, result){          
          var my_filter = request.body.filters;
          my_filter.map(function(ele){
            var filter = {
                filters_id_filters     : ele,
                companies_id_companies :result.insertId,
              };
              db.query('INSERT INTO filters_has_companies SET ?', filter, function(error, result_filter){
                response.json(result_filter);
              });             
          });
        });
    });
    db.end();
};

company.updateCompany = function(request, response) {		
  var user = {
    email    : request.body.email,
    password : request.body.password,
    name     : request.body.name,
    type   : 2
  };
  db.connect();
    db.query('UPDATE users SET ? WHERE users.type = 2 AND users.id_users=?',[user, request.params.companyId],
      function(error, result){        
        var company = {
          cep: request.body.cep,
          number: request.body.number,
          neighborhood: request.body.neighborhood,
          street: request.body.street,
          city : request.body.city,
          uf: request.body.uf,
          country: request.body.country,
          lat: 0,
          long: 0,
          phone: request.body.phone,
          price_around: request.body.price_around,
          users_id_users: result.insertId,
        };

        db.query('UPDATE companies SET ? WHERE companies.users_id_users=?',[company, request.params.companyId],function(error, result){
            var my_filter = request.body.filters;
            my_filter.map(function(ele){
              var filter = {
                  filters_id_filters     : ele,
                  companies_id_companies : request.params.companyId,
                };
                db.query("DELETE FROM filters_has_companies WERE companies_id_companies="+request.params.companyId, function(erro, delet){
                  db.query('INSERT INTO filters_has_companies SET ?', filter, function(error, result_filter){
                    response.json(result_filter);
                  });             
                });                  
            });
        });
    });
  db.end();
};

company.removeCompany = function(request, response) {	
	var user = { status : 0};
  db.connect();
	db.query('UPDATE users SET ? WHERE users.type = 2 AND users.id_users=?',[user, request.params.companyId], function(error, result){
      response.json(result);
  });
  db.end();
};

module.exports = company;