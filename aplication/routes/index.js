const BASE_URL = '/API';

var user       = require('../user'),       path = require('path');
var company    = require('../company'),    path = require('path');
var filter     = require('../filter'),     path = require('path');
var evaluation = require('../evaluation'), path = require('path');


module.exports  = function(app) {    
    app.route(BASE_URL)
        .get(company.getAllCompany);

    app.route(BASE_URL+'/users/')
        .get(user.getAllUser) 
        .post(user.addUser);

    app.route(BASE_URL+'/login/')
        .post(user.login);

    app.route(BASE_URL+'/users/:userId')
        .get(user.getUser)        
        .put(user.updateUser)
        .delete(user.removeUser);   


    app.route(BASE_URL+'/companies/')
        .get(company.getAllCompany)
        .post(company.addCompany);   

    app.route(BASE_URL+'/companies/filters')
        .post(company.getCompaniesByFilter)
    
    app.route(BASE_URL+'/companies/:companyId')
        .get(company.getCompany)
        .put(company.updateCompany)
        .delete(company.removeCompany);


    app.route(BASE_URL+'/filters')
        .get(filter.getAllFilters);

    app.route(BASE_URL+'/evaluations/')
        .post(evaluation.addEvaluation)  
        .get(evaluation.getEvaluationByUser);

    app.route(BASE_URL+'/evaluations/:evaluationId')
        .delete(evaluation.removeEvaluation)
        .put(evaluation.updateEvaluation);

    // habilitando HTML5MODE
    // app.all('/*', function(request, response) {
    //     response.sendFile(path.resolve('index.html'));
    // });
};
