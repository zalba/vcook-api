var express = require('express')
    ,app = express()
    ,bodyParser = require('body-parser')
    ,routes = require('../aplication/routes');


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Credentials", true);

  next();
});

app.use(express.static('/'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


routes(app);

module.exports = app;